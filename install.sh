#!/usr/bin/env bash

# Variables
user="steam"
BIND_IP="0.0.0.0"
PUBLIC_IP=$(curl -s http://whatismyip.akamai.com/)


# Other Vars
METAMOD_VERSION="1.10"
SOURCEMOD_VERSION="1.10"
# Download latest stop script
# curl --silent --output "stop.sh" "https://raw.githubusercontent.com/kus/csgo-modded-server/master/stop.sh" && chmod +x stop.sh

# Functions
log() {
  echo "${1}"
}
error() {
  echo "***************************************"
  echo "ERROR: ${1}"
  echo "***************************************"
  exit 1
}
# Check distrib
if ! command -v apt-get &> /dev/null; then
	error "OS distribution not supported..."
fi

# Check root
if [ "$EUID" -ne 0 ]; then
	error "Please run this script as root..."
fi

if [ -z "$PUBLIC_IP" ]; then
	error "Cannot retrieve your public IP address..."
fi

log "Updating Operating System..."
apt update -y -q
if ! apt upgrade -y; then
	error "Updating Operating System..."
fi

log "Adding i386 architecture..."
if ! dpkg --add-architecture i386 ; then
	error "Cannot add i386 architecture..."
fi

log "Installing required packages..."
apt-get update -y
if ! apt-get install -y libc6-i386 lib32stdc++6 lib32gcc1 lib32ncurses5 lib32z1 wget gdb screen tar unzip nano ; then
	error "Cannot install required packages..."
fi

log "Checking $user user exists..."
if ! getent passwd ${user} ; then
  log "Please Choose a password for the new user: ${user}"
  read -r -s -p "Password:" pass
  passhash=$(echo "${pass}" | mkpasswd -m sha-512 -s)
	log "Adding $user user..."
  log "useradd -m -d /home/${user} -p \"${passhash}\" -s /bin/bash -U"
  if ! useradd -m -d /home/${user} -p "${passhash}" -s /bin/bash -U ${user}; then
    error "Could not create user!"
  fi
  if ! usermod -a -G sudo ${user} ; then
    error "Failed to add ${user} to the 'sudo' group"
  fi
  if ! usermod -a -G tty ${user} ; then
    error "Failed to add ${user} to the 'tty' group"
  fi
fi

cd /home/${user} || error "Cannot cd into /home/${user}"

log "Downloading LGSM..."
if ! wget -O linuxgsm.sh https://linuxgsm.sh ; then
  error "Could Not download LGSM"
fi
if ! chmod +x linuxgsm.sh ; then
  error "Could not set exec on linuxgsm.sh"
fi
if ! chown ${user}:${user} linuxgsm.sh ; then
  error "Could not set ownership of linuxgsm.sh"
fi
su - -c 'bash linuxgsm.sh csgoserver' ${user}
su - -c './csgoserver auto-install' ${user}
log "Installing Mods..."
log "-> Insalling MetaMod..."
LATESTMM=$(wget -qO- https://mms.alliedmods.net/mmsdrop/"${METAMOD_VERSION}"/mmsource-latest-linux)
su - -c "wget -qO- https://mms.alliedmods.net/mmsdrop/\"${METAMOD_VERSION}\"/\"${LATESTMM}\" | tar xvzf - -C \"/home/${user}/serverfiles/csgo\"" ${user}
log "-> Installing SourceMod..."
LATESTSM=$(wget -qO- https://sm.alliedmods.net/smdrop/"${SOURCEMOD_VERSION}"/sourcemod-latest-linux)
su - -c "wget -qO- https://sm.alliedmods.net/smdrop/\"${SOURCEMOD_VERSION}\"/\"${LATESTSM}\" | tar xvzf - -C \"/home/${user}/serverfiles/csgo\"" ${user}
# log "-> Installing WarMod..."
# su - iC "wget -O /home/${user}/serverfiles/csgo/addons/sourcemod/plugins/warmod.smx https://warmod.bitbucket.io/plugins/warmod.smx" ${user}


# wget --quiet https://github.com/kus/csgo-modded-server/archive/master.zip
# unzip -o -qq master.zip
# cp -rlf csgo-modded-server-master/csgo/ /home/${user}/csgo/
# rm -r csgo-modded-server-master master.zip

echo "Dynamically writing /home/${user}/lgsm/config-lgsm/csgoserver/csgoserver.cfg"
# echo "rcon_password						\"$RCON_PASSWORD\"" > /home/${user}/lgsm/config-lgsm/csgoserver/csgoserver.cfg
echo "gslt=\"$STEAM_ACCOUNT\"" >> /home/${user}/lgsm/config-lgsm/csgoserver/csgoserver.cfg
echo "tickrate=\"128\"" >> /home/${user}/lgsm/config-lgsm/csgoserver/csgoserver.cfg
# if [ -z "$SERVER_PASSWORD" ]; then
# 	echo "sv_password							\"\"" >> /home/${user}/lgsm/config-lgsm/csgoserver/csgoserver.cfg
# else
# 	echo "sv_password							\"$SERVER_PASSWORD\"" >> /home/${user}/lgsm/config-lgsm/csgoserver/csgoserver.cfg
# fi
# if [ "$LAN" = "1" ]; then
# 	echo "sv_lan								1" >> /home/${user}/lgsm/config-lgsm/csgoserver/csgoserver.cfg
# else
# 	echo "sv_lan								0" >> /home/${user}/lgsm/config-lgsm/csgoserver/csgoserver.cfg
# fi
echo "sv_lan								0" >> /home/${user}/lgsm/config-lgsm/csgoserver/csgoserver.cfg
echo "sv_downloadurl						\"https://raw.githubusercontent.com/kus/csgo-modded-server-assets/master/csgo\"" >> /home/${user}/lgsm/config-lgsm/csgoserver/csgoserver.cfg
echo "sv_allowupload						0" >> /home/${user}/lgsm/config-lgsm/csgoserver/csgoserver.cfg
# if [ -z "$FAST_DL_URL" ]; then
# 	# No Fast DL
# 	echo "sv_allowdownload					1			// If using Fast download change to 0" >> /home/${user}/lgsm/config-lgsm/csgoserver/csgoserver.cfg
# else
# 	# Has Fast DL
# 	echo "sv_allowdownload					0			// If using Fast download change to 0" >> /home/${user}/lgsm/config-lgsm/csgoserver/csgoserver.cfg
# fi
echo "sv_allowdownload					0			// If using Fast download change to 0" >> /home/${user}/lgsm/config-lgsm/csgoserver/csgoserver.cfg
# echo "" >> /home/${user}/lgsm/config-lgsm/csgoserver/csgoserver.cfg
# echo "echo \"env.cfg executed\"" >> /home/${user}/lgsm/config-lgsm/csgoserver/csgoserver.cfg

# Uncomment below for custom admins
echo "Dynamically writing /home/$user/csgo/csgo/addons/sourcemod/configs/admins_simple.ini"
echo "\"STEAM_0:1:15556062\"    \"9:z\" // PhysK" > /home/${user}/serverfiles/csgo/addons/sourcemod/configs/admins_simple.ini
echo "\"STEAM_0:1:4503874\"     \"8:z\" // Wiggles" >> /home/${user}/serverfiles/csgo/addons/sourcemod/configs/admins_simple.ini
# echo "\"STEAM_0:0:3\" \"8:z\" // Third user" >> /home/${user}/csgo/csgo/addons/sourcemod/configs/admins_simple.ini

# chown -R ${user}:${user} /home/${user}/csgo

# cd /home/${user}/csgo

# echo "Starting server on $PUBLIC_IP:$PORT"
# ./srcds_run \
#     -console \
#     -usercon \
#     -autoupdate \
#     -game csgo \
#     -tickrate $TICKRATE \
#     -port $PORT \
#     -maxplayers_override $MAXPLAYERS \
#     +game_type 0 \
#     +game_mode 0 \
#     +mapgroup mg_active \
#     +map de_dust2 \
#     +ip ${BIND_IP}